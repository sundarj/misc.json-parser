WHITESPACE_CHARS = ("\u0009", "\u000A", "\u000D", "\u0020")

def parse(s):
    pos = 0
    tokens = []
    while pos < len(s):
        match = ""
        while pos < len(s) and s[pos] in WHITESPACE_CHARS:
            match += s[pos]
            pos += 1
        if match:
            tokens.append(("whitespace", match))
        if s[pos:pos+4] == "true":
            tokens.append(("boolean", s[pos:pos+4]))
            pos += 4
    return tokens