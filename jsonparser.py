import re

token_definitions = [
    ("WHITESPACE", re.compile("(\u0009|\u000A|\u000D|\u0020)+")),
    ("BOOLEAN", re.compile("(false|true)")),
    ("NULL", re.compile("null")),
    ("OPEN_BRACKET", re.compile("\\[")),
    ("CLOSE_BRACKET", re.compile("\\]")),
    ("OPEN_CURLY", re.compile("\\{")),
    ("CLOSE_CURLY", re.compile("\\}")),
    ("COMMA", re.compile(",")),
    ("COLON", re.compile(":")),
    ("NUMBER", re.compile("\\-?(0|[1-9][0-9]*)(\\.[0-9]+)?([eE][+-]?[0-9]+)?")),
    ("STRING", re.compile('"([^\\\\"\u0000\u0001\u0002\u0003\u0004\u0005\u0006\u0007\u0008\u0009\u000a\u000b\u000c\u000d\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f]|\\\\["\\\\/bfnrt]|\\\\u[0-9A-Fa-f]{4})*"'))
]

def parse(s):
    tokens = []
    while len(s) > 0:
        match_found = False
        for token_name, token_regex in token_definitions:
            if match := re.match(token_regex, s):
                match_found = True
                s = s[match.end():]
                tokens.append((token_name, match.group()))
        if not match_found:
            print("Unexpected", s)
            break
    return tokens

if __name__ == '__main__':
    print(parse(input()))